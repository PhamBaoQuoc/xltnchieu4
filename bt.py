#bai 1
import numpy as np
print(np.__version__)
np.show_config()
#bai 2
import numpy as np
x = np.zeros(10)
print(x)
#--------------------------------
#bai 3
p= np.zeros(10)
print(p)
#------------------------------
#bai 13
import numpy as np
a=np.random.rand(10,10)
print(a)
min=np.min(a)
max=np.max(a)
print(" max la: ",max)
print(" min la: ",min)
#------------------------------
#Câu 4
    #import numpy as np
#------------------------------
#Câu 5
import numpy as np
vector5 = np.zeros(10)
vector5[4]=1
print(vector5)
print(vector5)
#--------------------------------
#Câu 6
t = np.arange(10,50)
print(t)
#----------------------------------
#Câu 7
r = np.arange(50)
r = r[::-1]
print(r)
#---------------------------------
#Câu 8
import numpy as np
q = np.arange(9).reshape(3,3)
print(q)
#-----------------------------------
#Câu 9
y = np.nonzero([1,2,0,0,4,0])
print(y)
#------------------------------------
#cau 10
j= np.zeros( (5, 6) )
print(j)
#----------------------------------
#cau 11
u = np.eye(3)
print(u)
#-----------------------------------
#cau 12
i = np.random.random((3,3,3))
print(i)
#------------------------------------
#cau 14
s = np.random.random(30)
M = s.mean()
print(M)
#----------------------------------
#cau 15
d = np.ones((10,10))
d[1:-1,1:-1] = 0
print(d)
#-------------------------------------
#cau 16
f = np.ones((5,5))
f = np.pad(f, pad_width=1, mode='constant', constant_values=0)
print(f)
#------------------------------------
#cau 18
g = np.diag(1+np.arange(4),k=-1)
print(g)
#---------------------------------------
#cau 19
h = np.zeros((8,8),dtype=int)
h[1::2,::2] = 1
h[::2,1::2] = 1
print(h)
#----------------------------------------
#cau 20
print(np.unravel_index(99,(6,7,8)))
#----------------------------------------
#cau 21
k = np.random.random((3,3,3))
print(k)
#-----------------------------------------
#cau 22
l = np.random.random((5,5))
l = (l - np.mean (l)) / (np.std (l))
print(l)
#-------------------------------------------
#cau 23
color = np.dtype([("r", np.ubyte, 1),
                  ("g", np.ubyte, 1),
                  ("b", np.ubyte, 1),
                  ("a", np.ubyte, 1)])
print(color)
#-----------------------------------------
#cau 24
c = np.dot(np.ones((5,3)), np.ones((3,2)))
print(c)
c = np.ones((5,3)) @ np.ones((3,2))
print(c)
#---------------------------------------------
#cau 25
v = np.arange(11)
v[(3 < v) & (v <= 8)] *= -1
print(v)
#------------------------------------------
#cau 27
b = np.random.uniform(-10,+10,10)
print (np.copysign(np.ceil(np.abs(b)),b))
#-----------------------------------------
#cau 28
Z1 = np.random.randint(0,10,10)
Z2 = np.random.randint(0,10,10)
print(np.intersect1d(Z1,Z2))
#-----------------------------------------
#cau 29
defaults = np.seterr(all="ignore")
n = np.ones(1) / 0
_ = np.seterr(**defaults)
with np.errstate(divide='ignore'):
    n = np.ones(1) / 0
#--------------------------------------------
#cau 30
yesterday = np.datetime64('today', 'D') - np.timedelta64(1, 'D')
today     = np.datetime64('today', 'D')
tomorrow  = np.datetime64('today', 'D') + np.timedelta64(1, 'D')


