print("-----------------------------")
import numpy as np
import matplotlib.pyplot as plt

n=50
x = np.arange(n)
y = 0.2*np.cos(x)
plt.plot(x,y,'r-',linewidth=2)
plt.show()