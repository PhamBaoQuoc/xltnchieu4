#bai 1
#%matplotlib inline
import numpy as np   # khai bao thu vien
import matplotlib.pyplot as plt  #gọi ra thư viện
A = .8
f = 5
t = np.arange(0,1,.01) # tạo khoảng thời gian t với t(0,1)
phi = np.pi/4          #tạo góc
x = A*np.cos(2*np.pi*f*t + phi)  #công thức
plt.plot(t,x)          #hiển thị biểu đồ
plt.axis([0,1,-1,1])   #tạo 2 trục x,y với x(0,1) , y(-1,1)
plt.xlabel('time in seconds')  # gán nhãn
plt.ylabel('amplitude')         # gán nhãn
plt.show()                      #show ra

#-------------------------------------------
#bai 2
A = .65
fs = 100
samples = 100   #gán samples=100
f = 5
phi = 0
n = np.arange(samples) #tạo ra 1 mảng có samples phần tử có giá trị từ 0-(samples-1)
T = 1.0/fs # Remember to use 1.0 and not 1!  # 1.0 để phân biệt với bản python 3
y = A*np.cos(2*np.pi*f*n*T + phi) # công thức
plt.plot(y) # hiển thị biểu đồ
plt.axis([0,100,-1,1]) # tạo 2 trục x(0,100), y(-1,1)
plt.xlabel('sample index')#đặt nhãn
plt.ylabel('amplitude')# đặt nhãn
plt.show()# show ra
#----------------------------------
#bai 3
A = .8
N = 100 # samples  gán N=100
f = 5
phi = 0
n = np.arange(N) # tạo mảng có N phần tử giá trị từ 0-(N-1)
y = A*np.cos(2*np.pi*f*n/N + phi) #công thức
plt.plot(y) #hiển thị biểu đồ
plt.axis([0,100,-1,1])# tạo 2 trục x(0,100) y(-1,1)
plt.xlabel('sample index')#đăt nhãn
plt.ylabel('amplitude')#đặt nhãn
plt.show()#show ra
#----------------------------------------
#bai 4
f = 3
t = np.arange(0,1,.01)   #tạo khoảng thời gian t với t(0,1)
phi = 0
x = np.exp(1j*(2*np.pi*f*t + phi))# công thức
xim = np.imag(x) #phần ảo của biến x
plt.figure(1) #tạo biểu đồ
plt.plot(t,np.real(x))  #hiển thị phần thực của x
plt.plot(t,xim)  # hiện thị phần ảo của x
plt.axis([0,1,-1.1,1.1]) #tạo 2 trục x,y
plt.xlabel('time in seconds')# đặt nhãn
plt.ylabel('amplitude')#đặt nhãn
plt.show()#show ra
#-------------------------------------
#bai5
f = 3
N = 100
fs = 100
n = np.arange(N)# tạo 1 mãng có N giá trị từ 0-(N-1)
T = 1.0/fs
t = N*T
phi = 0
x = np.exp(1j*(2*np.pi*f*n*T + phi))# công thức
xim = np.imag(x) # phần ảo của biến x
plt.figure(1)  # tạo biểu đồ
plt.plot(n*T,np.real(x)) #hiển thị phần thực
plt.plot(n*T,xim)       #hiển thị phần ảo
plt.axis([0,t,-1.1,1.1]) #tạo 2 trục x(0,t) y(-1.1,1.1)
plt.xlabel('t(seconds)') #đặt nhãn
plt.ylabel('amplitude')# đặt nhãn
plt.show()# show ra
#---------------------------------------------
#bai6
f = 3
N = 64
n = np.arange(64) #tạo mảng có 64 phần tử có gtri từ 0-63
phi = 0
x = np.exp(1j*(2*np.pi*f*n/N + phi))# công thức
xim = np.imag(x) #phần ảo của x
plt.figure(1)# tạo biểu đồ
plt.plot(n,np.real(x)) # hiển thị phần thực của x
plt.plot(n,xim)# hiển thị phần ảo của x
plt.axis([0,samples,-1.1,1.1]) #tạo 2 trục x(0,samples) y(-1.1,1.1)
plt.xlabel('sample index') #đặt nhãn
plt.ylabel('amplitude')  # đặt nhãn
plt.show()  # show ra
#---------------------------------------------------
#bài 7
N = 44100 # samples
f = 440
fs = 44100
phi = 0
n = np.arange(N)
x = A*np.cos(2*np.pi*f*n/N + phi)
#scipy.io.wavfile.write(filename, rate, data)[source]
from scipy.io.wavfile import write # tạo file
write('sine440_1sec.wav', 44100, x)# ghi dữ liệu vào x
